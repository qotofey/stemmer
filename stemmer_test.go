package stemmer

import (
	"testing"
)

func Test(t *testing.T) {
	var tests = []struct {
		word string
		want string
	}{
		// Широкая электрификация южных губерний даст мощный толчок подъёму сельского хозяйства.
		{"широкая", "широк"},
		{"электрификация", "электрификац"},
		{"южных", "южн"},
		{"губерний", "губерн"},
		{"даст", "даст"},
		{"мощный", "мощн"},
		{"толчок", "толчок"},
		{"подъёму", "подъ"},
		{"сельского", "сельск"},
		{"хозяйства", "хозяйств"},
		// Съешь же ещё этих мягких французских булок, да выпей чаю.
		{"съешь", "съеш"},
		{"же", "же"},
		{"ещё", "ещ"},
		{"этих", "эт"},
		{"мягких", "мягк"},
		{"французских", "французск"},
		{"булок", "булок"},
		{"да", "да"},
		{"выпей", "вып"},
		{"чаю", "ча"},
		// Мы летим на самолёте
		{"мы", "мы"},
		{"летим", "лет"},
		{"на", "на"},
		{"самолёте", "самолет"},
		//
		{"ё", "е"},
		{"евших", "евш"},
		{"выступала", "выступа"},
		{"выступавших", "выступа"},
		//
		{"богатства", "богатств"},
		{"обществ", "обществ"},
		{"в", "в"},
		{"которых", "котор"},
		{"господствует", "господств"},
		{"капиталистический", "капиталистическ"},
		{"способ", "способ"},
		{"производства", "производств"},
		{"выступает", "выступа"},
		{"как", "как"},
		{"огромное", "огромн"},
		{"скопление", "скоплен"},
		{"товаров", "товар"},
		{"а", "а"},
		{"отдельный", "отдельн"},
		{"товар", "товар"},
		{"элементарная", "элементарн"},
		{"форма", "форм"},
		{"этого", "эт"},
		{"богатства", "богатств"},
	}

	for _, test := range tests {
		got := StemRu(test.word)
		if got != test.want {
			t.Errorf("StemRu(%q) == %q, want %q", test.word, got, test.want)
		}
	}

}
