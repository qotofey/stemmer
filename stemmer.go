package stemmer

import (
	"regexp"
	"strings"
)

var firstSyllableRx = regexp.MustCompile(`^(.*?[аеиоуыэюя])(.*)$`)

var perfectiveGroundRx = regexp.MustCompile(`^(?P<base>[а-я]+)(ив|ивши|ившись|ыв|ывши|ывшись)|(?P<saved>[ая])(в|вши|вшись)$`)

var reflexiveRx = regexp.MustCompile(`(с[яь])$`)
var adjectiveRx = regexp.MustCompile(`(ее|ие|ые|ое|ими|ыми|ей|ий|ый|ой|ем|им|ым|ом|его|ого|ему|ому|их|ых|ую|юю|ая|яя|ою|ею)$`)

var participleRx = regexp.MustCompile(`^(?P<base>[а-я]+)(ивш|ывш|ующ)|(?P<saved>[ая])(ем|нн|вш|ющ|щ)$`)
var verbRx = regexp.MustCompile(`(?P<base>[а-я]+)(ила|ыла|ена|ейте|уйте|ите|или|ыли|ей|уй|ил|ыл|им|ым|ен|ило|ыло|ено|ят|ует|уют|ит|ыт|ены|ить|ыть|ишь|ую|ю)|(?P<saved>[ая])(ла|на|ете|йте|ли|й|л|ем|н|ло|но|ет|ют|ны|ть|ешь|нно)$`)

var nounRx = regexp.MustCompile(`(а|ев|ов|ие|ье|е|иями|ями|ами|еи|ии|и|ией|ей|ой|ий|й|иям|ям|ием|ем|ам|ом|о|у|ах|иях|ях|ы|ь|ию|ью|ю|ия|ья|я)$`)
var derivativeRx = regexp.MustCompile(`.*[^аеиоуыэюя]+[аеиоуыэюя].*ость?$`)
var derRx = regexp.MustCompile(`ость?$`)
var superlativeRx = regexp.MustCompile(`(ейше?)$`)
var iRx = regexp.MustCompile(`и$`)
var pRx = regexp.MustCompile(`ь$`)
var nnRx = regexp.MustCompile(`нн$`)

func StemRu(word string) string {
	//заменяем все буквы 'ё' на 'е'
	result := strings.ReplaceAll(word, "ё", "е")
	//находим первый слог и остальной хвост слова
	if matches := firstSyllableRx.FindAllStringSubmatch(result, -1); matches != nil {
		if len(matches[0]) > 0 {
			syllable := matches[0][1] 	//первый слог
			tail := matches[0][2]		//хвост слова
			if temp := perfectiveGroundRx.ReplaceAllString(tail, "${base}${saved}"); tail == temp {
				tail = reflexiveRx.ReplaceAllString(tail, "")
				temp = adjectiveRx.ReplaceAllString(tail, "")
				if temp != tail {
					tail = temp
					tail = participleRx.ReplaceAllString(tail, "${base}${saved}")
				} else {
					temp = verbRx.ReplaceAllString(tail, "${base}${saved}")
					if temp == tail {
						tail = nounRx.ReplaceAllString(tail, "")
					} else {
						tail = temp
					}
				}
			} else {
				tail = temp
			}
			tail = iRx.ReplaceAllString(tail, "")
			if derivativeRx.MatchString(tail) {
				tail = derRx.ReplaceAllString(tail, "")
			}
			if temp := pRx.ReplaceAllString(tail, ""); temp == tail {
				tail = superlativeRx.ReplaceAllString(tail, "")
				tail = nnRx.ReplaceAllString(tail, "н")
			} else {
				tail = temp
			}
			result = syllable + tail
		}
	}
	return result
}
